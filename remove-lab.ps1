<#
SCRIPT WRITTEN BY : Dave Hocking, Cloud Architect, Nutanix GSI Team, UK
SCRIPT WRITTEN ON : First created, April 21st 2021

DESCRIPTION : This script is part 2/2, with the 1st part creating the
              environment objects in AWS, that this script is designed
              to clean up. Should the provisioning operation fail,
              this script can be used to clean up the AWS account,
              prior to making another attempt.
#>

# Allow advanced parameter functions
[CmdletBinding()]
Param(
    [Parameter(Mandatory=$false)]
    [string]$projectName="sf-autobuild"
    )

# Clear the screen to format output nicely
Clear-Host

# Setup the module
initialize-module

# Insert line spacing
Write-Host ""
    
# Setup Global Vars
$domainName = "$projectName"+".aws"

# Get the VPC Object
Write-Host "Checking for VPC... " -NoNewline
$oldVpc = Get-EC2Vpc | Where-Object {$_.tag.value -eq $projectName}
if (!$oldvpc){
    Write-Host ""
    Write-Error "No VPC found"
}
else {
    done





    <#
    
    work out the order of removal before you update any more code!!

WATCH THE ORDER THAT AMAZON GOES THROUGH WHEN REMOVING VPC, IF YOU HAVE TO MANUALLY REMOVE ALL DEPENDANT COMPONENTS

    - VPC & Constructs
        
        Delete ELBs
        Delete the NAT GW
        # Delete the Subnets - done automatically?
        Release the Elastic IP with no Private IP Mapping
        # Detach internet gateway from vpc - done automatically?
        # Delete internet gateway - done automatically?
        VPC
        KeyPair cleanup - prompt for this (Remove-EC2KeyPair -KeyName $projectName -force)
    
    - Route53
        Delete the records first, then the zone?
    
    #>





    # Fetch, dismount and delete any NAT gateway IDs, attached to the old VPC
    Write-Host "Checking for NAT Gateways... " -NoNewline
    $natGateway = Get-EC2NatGateway | Where-Object {$_.VpcId -eq $oldVPC.VpcId}
    if (!$natGateway){
        Write-Warning "No NAT gateways found"
    }
    else {
    
        Write-Host "We found these NAT Gateways..."
        Write-Host $natGateway

        # Get the state of any NAT gateways, wait until they're "available" before removing them
        if($natGateway.State -eq "available"){
            # Reset the success var, use this to check for successful dismount/remove
            $success = $null

            # Get the ElasticIP of the NAT gateway to delete
            $natPublic = $natGateway.NatGatewayAddresses.publicip
            $natAllocationID = $natGateway.NatGatewayAddresses.allocationid

            # Delete the NAT Gateway
            Write-Host "Removing NAT Gateway with address " -NoNewline
            Write-Host "$natPublic " -NoNewline -ForegroundColor Yellow
            write-host "..."
            $success = Remove-EC2NatGateway -NatGatewayId $natGateway.NatGatewayId -Force
            
            # Check for success
            if (!$success){
                Write-Warning "Couldn't remove the NAT Gateway"
            }
            else{
                done
            }

            # Needs some work here - perhaps just trash all allocated addresses that aren't mapped?

            # Release the ElasticIP reservations
            Write-Host "Releasing ElasticIP... " -NoNewline
            Remove-EC2Address -AllocationId $natAllocationID -Force
            done

            # Fetch, dismount and delete any internet gateway IDs, attached to the old VPC
            Write-Host "Checking for Internet Gateways... " -NoNewline
            $internetGateway = Get-EC2InternetGateway | Where-Object {$_.Attachments.VpcId -eq $oldVpc.VpcId}
            if (!$internetGateway){
                Write-Warning "No internet gateways found"
            }
            else {
                done
                
                # Reset the success var, use this to check for successful dismount/remove
                $success = $null

                Write-Host "Disconnecting Internet Gateways... " -NoNewline
                $success = Dismount-EC2InternetGateway -InternetGatewayId $internetGateway.InternetGatewayId -VpcId $oldVpc.VpcId
                done

                Write-Host "Removing Internet Gateways... " -NoNewline
                $success = Remove-EC2InternetGateway -InternetGatewayId $internetGateway.InternetGatewayId -Force
                done

                # Check for success var, if it exists, we have a problem
                if ($success){
                    Write-Error "Couldn't detach/remove gateway"
                }

                # Must remove Subnets before the VPC
                $subnetsToDelete = Get-EC2Subnet | Where-Object {$_.VpcId -eq $oldVpc.VpcId}

                foreach ($subnet in $subnetsToDelete)
                {
                    Write-Host "Deleting Subnet " -NoNewline
                    Write-Host $subnet.tags.value -ForegroundColor Yellow -NoNewline
                    Write-Host "... " -NoNewline
                    Remove-EC2Subnet -SubnetId $subnet.SubnetId -Force
                    done
                }

                # Remove the VPC
                Write-Host "Removing VPC : " -NoNewline
                Write-Host "$projectName" -ForegroundColor Yellow
                Remove-EC2Vpc -VpcId $oldVpc.VpcId -Force

            }
        }
        
        else{
            Write-Warning "Cannot remove the NAT gateway as it's not 'available'"
        }
    }
}


# Get the Route53 Object
Write-Host "Checking for Route53 Hosted Zone... " -NoNewline
$r53hostedZone = Get-R53HostedZones | Where-Object {$_.Name -match $domainName}
if (!$r53hostedZone){
    Write-Host ""
    Write-Error "No Route 53 Zone found"
}
else {
    done
    Write-Host "Removing Route53 Hosted Zone... " -NoNewline
    $removalTask = Remove-R53HostedZone -Id $r53hostedZone.Id -Force
    if (!$removalTask -or $removalTask.Status -ne "PENDING"){
        Write-Error "Couldn't remove zone"
    }
    else {
        done
    }
}
