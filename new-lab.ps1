<#
.SYNOPSIS
MODULE WRITTEN BY : Dave Hocking, Cloud Architect, Nutanix GSI Team, UK
MODULE WRITTEN ON : First created, April 21st 2021

Please review the help files contained in the module file, "aws-autolab.psm1".

This script will automate the many steps needed to create a fully functioning,
self-contained lab environment in AWS. Namely:

Create VPC with CIDR
Attach Internet Gateway
Set VPC to DNS Hostname enable
Create Route53 Hosted Domain
Create Subnets for Uplink, Management and UserVMs
Create NAT Gateway
Attach NAT Gateway to the Uplink Subnet
Create Separate Route Table for Uplink, add default route to the Internet Gateway
Create Separate Route Table for Internal Subnets, add default route to the NAT gateway
Create Security Group allowing all internal-internal traffic (incl) VPN Users
Create SSH Keypair for Instances, if needed
Deploy EC2 t2.micro instance (free tier) to create VPN Keys/Certs
    - assign to securitygroup
    - create route53 record for "ec2linux."
Deploy Elastic Load Balancers, to allow inbound, "public" access to services
    - e.g. Publishing Prism Central (and filtering inbound access through an ACL)
    - ..because not everyone will have the ability to run the OpenVPN client
    - ..and a Classic ELB for SSH access to ec2linux box
Create AWS Certificate Store configuration and import CA,Server and Client certs
Create Client VPN Endpoint using mutual auth and specifying DNS Server
    - Associate with the UserVMs network
    - Authorise all users for access to the whole VPC CIDR
    - Attach a virtual private gateway

Show deployment instructions for OpenVPN client and connection file

.DESCRIPTION

File Descriptions
=================

FILENAME            PURPOSE
aws-autolab.psm1    Module file, contains functions used in the scripts below
new-lab.ps1         This file, creates the lab environment
remove-lab.ps1      This script is responsible for cleaning up





.LINK
https://bitbucket.org/davehocking/aws-autolab/src/main/

#>

# Allow advanced parameter functions
[CmdletBinding()]
Param(
    [Parameter(Mandatory=$false)]
    # ProjectName needs to be between 3 and 16 chars of alphanumeric (lower case) and hyphens
    [ValidatePattern("^[a-z\d\-]{3,16}$")]
    [string]$projectName="aws-autobuild",

    [Parameter(Mandatory=$false)]
    # first 2 Octets needs to match the correct formatting for ipv4
    [ValidatePattern("^(?:25[0-5]|2[0-4]\d|[0-1]?\d{1,2})\.(?:25[0-5]|2[0-4]\d|[0-1]?\d{1,2})$")]
    [string]$first2Octect="172.31",

    [Parameter(Mandatory=$false)]
    # It would be good to replace this validation pattern with a helpful message instead of the default "didn't match" message
    [ValidatePattern("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/(3[0-2]|[1-2][0-9]|[0-9]))$")]
    [string]$sourceCIDR
    )

# Clear the screen to format output nicely
Clear-Host

# Insert banner
show-Banner

# Insert line spacing
Write-Host ""

# Check for correct PS Edition
initialize-module

# Insert line spacing
Write-Host ""

# Hash Table for subnet creation later on
$subnetsToCreate = @{
    'uplink' = $first2Octect+'.0.0/24' # This name should stay the same, as it's used for the NAT gateway creation later
    'ntnx-mgmt' = $first2Octect+'.1.0/24'
    'user-vms' = $first2Octect+'.2.0/24'
    'services' = $first2Octect+'.3.0/24'
}
    
# Setup Global Vars
$domainName = "$projectName"+".aws"
$nameServer = $first2Octect+".0.2"
$vpcCIDR = $first2Octect+".0.0/22"
$uplinkCIDR = ($subnetsToCreate.GetEnumerator() | Where-Object {$_.name -eq "uplink"}).value
$ntnxmgmtCIDR = ($subnetsToCreate.GetEnumerator() | Where-Object {$_.name -eq "ntnx-mgmt"}).value
$userVmCIDR = ($subnetsToCreate.GetEnumerator() | Where-Object {$_.name -eq "user-vms"}).value
$servicesCIDR = ($subnetsToCreate.GetEnumerator() | Where-Object {$_.name -eq "services"}).value
$vpnCIDR = $first2Octect+".4.0/22"
$ec2LinuxVMname = "ec2linux"

# Output Summary
Write-Host "We'll be building the following constructs in"$region"... "
Write-Host ""
Write-Host "VPC : " -NoNewline
Write-Host $projectName -ForegroundColor Yellow
Write-Host "CIDR : " -NoNewline
Write-Host $vpcCIDR -ForegroundColor Yellow
Write-Host "    ----------------------------------------- "
Write-Host "   | " -NoNewline
Write-Host "Internet Gateway" -NoNewline -ForegroundColor DarkRed
Write-Host "                        |"
Write-Host " --" -NoNewline
Write-Host "O" -NoNewline -ForegroundColor Red
Write-Host "--------------------" -NoNewline
Write-Host "Uplink Subnet" -NoNewline -ForegroundColor Magenta
Write-Host "        |"
Write-Host "   |                   |" -NoNewline
Write-Host "$uplinkCIDR" -NoNewline -ForegroundColor DarkMagenta
Write-Host "        |"
Write-Host "   |                   |                     |"
Write-Host "   |       " -NoNewline
Write-Host "NAT Gateway O" -NoNewline -ForegroundColor Yellow
Write-Host "----" -NoNewline
Write-Host "NTNX Mgmt Subnet" -NoNewline -ForegroundColor Cyan
Write-Host " |"
Write-Host "   |                  / \   " -NoNewline
Write-Host "$ntnxmgmtCIDR" -NoNewline -ForegroundColor DarkCyan
Write-Host "    |"
Write-Host "   |                  | |                    |"
Write-Host "   |                  |  ---" -NoNewline
Write-Host "User VMs Subnet" -NoNewline -ForegroundColor Green
Write-Host "  |"
Write-Host "   |                  |     " -NoNewline    
Write-Host "$userVmCIDR" -NoNewline -ForegroundColor DarkGreen
Write-Host "    |"
Write-Host "   |                  |                      |"
Write-Host "   |                   -----" -NoNewline
Write-Host "Services Subnet" -NoNewline -ForegroundColor Blue
Write-Host "  |"
Write-Host "   |                        " -NoNewline
Write-Host "$servicesCIDR" -NoNewline -ForegroundColor DarkBlue
Write-Host "    |"
Write-Host "   |                                         |"
Write-Host "    ----------------------------------------- "
Write-Host ""
Write-Host "OpenVPN User Endpoint CIDR : " -NoNewline
Write-Host "$vpnCIDR" -ForegroundColor Yellow
Write-Host "Route 53 Hosted Domain Name : " -NoNewline
Write-Host "$domainName" -ForegroundColor Yellow
Write-Host ""

# Check if the user wants to run from here
confirm-continue
if ($break) {
    break
}

# Check for existing VPC with matching name/CIDR - while it's possible to have more than one, it's likely an error in the script
Write-Host "Checking for conflicts with existing VPC constructs... " -NoNewline
if ((Get-EC2Vpc | Where-Object {$_.tags.value -eq $projectName}) -or (Get-EC2Vpc | Where-Object {$_.CidrBlock -eq $vpcCIDR})){
    Write-Host ""
    Write-Error "Conflicting VPC name or CIDR within Region, please change values" -ErrorAction Stop
}
else {
    done
}

# Create a new VPC
Write-Host "Building new VPC to spec... " -NoNewline
if (!($newVPC = New-EC2VPC -CidrBlock $vpcCIDR)){
    Write-Host ""
    Write-Error "Couldn't build new VPC" -ErrorAction stop
}
else {
    done
}

# Create a new EC2 Tag for the VPC name
Write-Host "Assigning Name Tag... " -NoNewline
$tag = New-Object Amazon.EC2.Model.Tag
    $tag.Key = "Name"
    $tag.Value = $projectName

# ...and assign it to the VPC
New-EC2Tag -Resource $newVPC.VpcId -Tag $tag

# Check if it's been applied properly
$taggedVPC = Get-EC2Tag | Where-Object {$_.ResourceType -eq "vpc" -and $_.Value -eq $projectName}
if (!$taggedVPC){
    Write-Host ""
    Write-Error "Couldn't tag VPC" -ErrorAction stop
}
else {
    done
}

# Insert line spacing
Write-Host ""

# Check if the VPC has dnsHostnames support active - default is false
Write-Host "Checking for DNS Hostname Support... " -NoNewline
$vpcDnsAttributes = Get-EC2VpcAttribute -VpcId $newVPC.VpcId -Attribute EnableDnsHostnames
if ($vpcDnsAttributes.EnableDnsHostnames -eq $false) {
    #Write-Warning "DNS Hostname support missing"
    # Attempt to fix, and verify
    Edit-EC2VpcAttribute -EnableDnsHostname $true -VpcId $newVPC.VpcId
    $vpcDnsAttributes = Get-EC2VpcAttribute -VpcId $newVPC.VpcId -Attribute enableDnsHostnames
    if ($vpcDnsAttributes.EnableDnsHostnames -eq $false) {
        Write-Error "Attempt to fix DNS Hostname support on the VPC failed - aborting" -ErrorAction Stop
    }
    else {
        Write-Host "Added" -ForegroundColor Green
    }       
}
else {
    Write-Host "Exists" -ForegroundColor Green
}

# Check if the VPN has DNS Resolution support - default is true
Write-Host "Checking for DNS Resolution Support... " -NoNewline
if ($vpcDnsAttributes.EnableDnsSupport -eq $false) {
    #Write-Warning "DNS Resolution Support missing"
    # Attempt to fix, and verify
    Edit-EC2VpcAttribute -EnableDnsSupport $true -VpcId $newVPC.VpcId
    $vpcDnsAttributes = Get-EC2VpcAttribute -VpcId $newVPC.VpcId -Attribute enableDnsSupport
    if ($vpcDnsAttributes.EnableDnsSupport -eq $false) {
        Write-Error "Attempt to fix DNS Resolution support on the VPC failed - aborting" -ErrorAction Stop     
    } 
    else {
        Write-Host "Added" -ForegroundColor Green
    }    
} 
else {
    Write-Host "Exists" -ForegroundColor Green
}

# Insert line spacing
Write-Host ""

# Create a Route53 Hosted Domain
Write-Host "Creating Route53 Hosted Domain... " -NoNewline
$timestamp = Get-Date -Format o | ForEach-Object { $_ -replace ":", "." }
$newR53zone = New-R53HostedZone -VPC_VPCId $newVPC.VpcId -VPC_VPCRegion $region -Name $domainName -CallerReference $timestamp
if (!$newR53zone){
    Write-Host ""
    Write-Error "Couldn't build Route53 Hosted Zone" -ErrorAction Stop
}
else {
    done
}

# Insert line spacing
Write-Host ""

# Create subnets for Uplinks, User VMs, Management and Services
foreach ($subnet in $subnetsToCreate.GetEnumerator() )
{
    Write-Host "Creating Subnet for " -NoNewline
    Write-Host "$($subnet.Name)" -NoNewline -ForegroundColor Yellow
    Write-Host "... " -NoNewline
    $createdSubnet = New-EC2Subnet -VpcId $newVPC.VpcId -CidrBlock $($subnet.Value)
    if (!$createdSubnet){
        Write-Host ""
        Write-Error "Failed to create Subnet"
    }
    else {
        done
        # Apply tag to the new subnet
        Write-Host "Assigning Name Tag... " -NoNewline
        $tag = New-Object Amazon.EC2.Model.Tag
            $tag.Key = "Name"
            $tag.Value = $subnet.Name

        # ...and assign it to the VPC
        New-EC2Tag -Resource $createdSubnet.SubnetId -Tag $tag

        # Check if it's been applied properly
        $taggedSubnet = Get-EC2Tag | Where-Object {$_.ResourceType -eq "subnet" -and $_.Value -eq $subnet.Name}
        if (!$taggedSubnet){
            Write-Host ""
            Write-Error "Couldn't tag Subnet" -ErrorAction stop
        }
        else {
            done
        }
    }
}

# Insert line spacing
Write-Host ""

# Create Internet Gateway
Write-Host "Creating Internet Gateway... " -NoNewline
$newInternetGateway = New-EC2InternetGateway
if (!$newInternetGateway) {
    Write-Error "Cannot create internet gateway" -ErrorAction Stop
}
else {
    done
}
# Create and Assign tags to new gateway
Write-Host "Assigning Name Tag... " -NoNewline
$tag = New-Object Amazon.EC2.Model.Tag
    $tag.Key = "Name"
    $tag.Value = $projectName

# ...and assign it to the VPC
New-EC2Tag -Resource $newInternetGateway.InternetGatewayId -Tag $tag
done

# Check if it's been applied properly
$taggedGateway = Get-EC2Tag | Where-Object {$_.ResourceType -eq "internet-gateway" -and $_.Value -eq $projectName}
if (!$taggedGateway){
    Write-Host ""
    Write-Error "Couldn't tag Gateway" -ErrorAction stop
}
else {
    # Attach Internet Gateway to VPC
    Write-Host "Attaching Internet Gateway... " -NoNewline
    Add-EC2InternetGateway -VpcId $newVPC.VpcId -InternetGatewayId $newInternetGateway.InternetGatewayId
    
    $attachedInternetGateway = Get-EC2InternetGateway | Where-Object {$_.Attachments.VpcId -eq $newVPC.VpcId}
    if (!$attachedInternetGateway){
        Write-Host ""
        Write-Error "Couldn't attach Gateway" -ErrorAction stop
    }
    else {
        done
    }
}

# Insert line spacing
Write-Host ""

# Allocate a new Elastic IP
Write-Host "Allocating new ElasticIP for NAT..." -NoNewline
$elasticIPforNAT = New-EC2Address

# Check it's worked and proceed if good
if (!$elasticIPforNAT){
    Write-Error "Couldn't allocate an ElasticIP for NAT purposes" -ErrorAction Stop
}
else {
    done

    # Create NAT Gateway, and attach it to the uplink subnet, using the new ElasticIP
    Write-Host "Creating NAT Gateway " -NoNewline 
    Write-Host $elasticIPforNAT.PublicIp -ForegroundColor Yellow -NoNewline
    Write-Host " on uplink subnet... " -NoNewline
    $uplinkSubnet = Get-EC2Subnet | Where-Object {$_.Tags.value -eq "uplink" -and $_.VpcId -eq $newVPC.VpcId}
    $newNATGateway = New-EC2NatGateway -SubnetId $uplinkSubnet.SubnetId -AllocationId $elasticIPforNAT.allocationid
    if (!$newNATGateway) {
        Write-Error "Cannot create NAT gateway" -ErrorAction Stop
    }
    else {
        done
    }

    # Wait for the NAT Gateway to be available
    Write-Host "Waiting for NAT Gateway creation to complete... "
    Test-ObjectStatus -id $newNATGateway.natgateway.NatGatewayId -wait -for available -mins 4

    # Create and Assign tags to new gateway
    Write-Host "Assigning Name Tag... " -NoNewline
    $tag = New-Object Amazon.EC2.Model.Tag
        $tag.Key = "Name"
        $tag.Value = $projectName

    # ...and assign it to the VPC
    New-EC2Tag -Resource $newNATGateway.NatGateway.NatGatewayId -Tag $tag
    done
}

# Insert line spacing
Write-Host ""

# Create a new Route Table for the Internet Access
Write-Host "Creating new Route Table " -NoNewline
Write-Host "Uplink" -NoNewline -ForegroundColor Yellow
Write-Host "... " -NoNewline
$newUplinkRouteTable = New-EC2RouteTable -VpcId $newVPC.VpcId
done

# Create and Assign tags to new gateway
Write-Host "Assigning Name Tag... " -NoNewline
$tag = New-Object Amazon.EC2.Model.Tag
    $tag.Key = "Name"
    $tag.Value = $projectName+"-uplink"

# And assign it to the route table
New-EC2Tag -Resource $newUplinkRouteTable.RouteTableId -Tag $tag
done

# Set the default route 0.0.0.0/0 to be via the internet Gateway
Write-Host "Creating default route entry... " -NoNewline
$newRoute = New-EC2Route -RouteTableId $newUplinkRouteTable.RouteTableId -DestinationCidrBlock 0.0.0.0/0 -GatewayId $newInternetGateway.InternetGatewayId

# Check for proper creation
if (!$newRoute){
    Write-Error "Couldn't create routing entry for new table" -ErrorAction stop
} 
else {
    done
}

# Associate the route-table with the Internet Gateway/Uplink Subnet and hide the output
Write-Host "Associating route table with Uplink Subnet... " -NoNewline
$null = Register-EC2RouteTable -RouteTableId $newUplinkRouteTable.RouteTableId -SubnetId $uplinkSubnet.SubnetId

# Check for association health
$uplinkAssociations = (Get-EC2RouteTable | Where-Object {$_.tags.value -eq $projectName+"-uplink"}).Associations       
if (($uplinkAssociations | Where-Object {$_.SubnetId -eq $uplinkSubnet.subnetId}).AssociationState.state.value -eq "associated"){
    done
}
else {
    Write-Error "Couldn't associate table to the uplink subnet" -ErrorAction stop
}

# Insert line spacing
Write-Host ""


# Create a new Route Table for the Internal Traffic
Write-Host "Creating new Route Table " -NoNewline
Write-Host "Internal" -NoNewline -ForegroundColor Yellow
Write-Host "... " -NoNewline
$newInternalRouteTable = New-EC2RouteTable -VpcId $newVPC.VpcId
done

# Create and Assign tags to new gateway
Write-Host "Assigning Name Tag... " -NoNewline
$tag = New-Object Amazon.EC2.Model.Tag
    $tag.Key = "Name"
    $tag.Value = $projectName+"-internal"

# And assign it to the route table
New-EC2Tag -Resource $newInternalRouteTable.RouteTableId -Tag $tag
done

# Set the default route 0.0.0.0/0 to be via the NAT Gateway
Write-Host "Creating default route entry... " -NoNewline

$natGateway = Get-EC2NatGateway | Where-Object {$_.vpcid -eq $newVPC.VpcId -and $_.tags.value -eq $projectName}
$newNATRoute = New-EC2Route -RouteTableId $newInternalRouteTable.RouteTableId -DestinationCidrBlock 0.0.0.0/0 -NatGatewayId $natGateway.NatGatewayId

# Check for proper creation
if (!$newNATRoute){
    Write-Error "Couldn't create routing entry for new table" -ErrorAction stop
} 
else {
    done
}

# Take a subset of the subnets we created initially, and remove the "uplink" network
$subnetsToCreate.Remove("uplink")

# Associate the route-table with the Internal Subnets
Write-Host "Associating route table with Internal Subnets... " -NoNewline

foreach ($subnet in $subnetsToCreate.GetEnumerator())
{
    # Fetch the subnet ID for each of the named subnets in the hash-table
    $subnetId = (Get-EC2Subnet | Where-Object {$_.VpcId -eq $newVPC.VpcId -and $_.tags.value -eq $subnet.Name}).subnetid

    Write-Host "$($subnet.Name)" -NoNewline -ForegroundColor Yellow
    $registration = Register-EC2RouteTable -RouteTableId $newInternalRouteTable.RouteTableId -SubnetId $subnetId

    # Check for association health
    if($registration){
        Write-Host ":" -NoNewline
        Write-Host "OK" -NoNewline -ForegroundColor green
        Write-Host ", " -NoNewline
    }
    else {
        Write-Error "Couldn't associate table to the $($subnet.Name) subnet" -ErrorAction stop
    }

    # Clear the registration variable for the next pass
    $registration = $null
}
done

# Insert line spacing
Write-Host ""

# Collect a list of keypairs (if any exist) and present option to use one of these keypairs (or create a new one)
Write-Host "Collecting EC2 Keypairs... " -NoNewline
$keyPairs = Get-EC2KeyPair
done

# Initialise the keyMenu variable (as an arraylist) with the option to create new
[System.Collections.ArrayList]$keyMenu = @("Create NEW keypair")

# Handle the situation where there are no existing keys
if (!$keyPairs){
    Write-Host "None Found " -ForegroundColor Yellow
}

# ..and when there are some
else{
    # Loop through the keypairs listed and add to the arraylist, to hold their names only
    foreach ($keyPair in $keyPairs) {
        $null = $keyMenu.Add($keyPair.keyName)
    }
}

# Insert line spacing
Write-Host ""

# Show the menu of choices from the keyMenu arraylist
Write-Host "Please select an EC2 keypair that you want to use."
show-arrayMenu(@($keyMenu))

# Rewrite the variable name to make things clear
$keyPairName = $object

# Handle the situation where a new keypair is needed
if ($keyPairName -eq "Create NEW keypair"){
    Write-Host "Creating new KeyPair... " -NoNewline
    $newKey = New-EC2KeyPair -KeyName $projectName
    done

    # Display the private key and copy it to the clipboard for the user to save as a PEM file
    Write-Host "Here's your private key, copied to the clipboard, " -NoNewline
    Write-Host "SAVE IT" -ForegroundColor white -BackgroundColor red -NoNewline
    Write-Host " as a .PEM file NOW!"
    Write-Host $newKey.keyMaterial -ForegroundColor DarkYellow
    $newKey.keyMaterial | Set-Clipboard

    # Check progress
    Write-Host "Proceed once you have saved the file."
    Pause

    # Set the variable we'll use to track keyPair Name selection
    $keyPairName = $newKey.keyname
}

# Get the KeyPair object for the chosen keypair
Write-Host "Getting KeyPair object selected... " -NoNewline
$keyPair = Get-EC2KeyPair -keyName $keyPairName
done

# Insert line spacing
Write-Host ""

# Create the internal security group, that describes traffic coming from inside the lab/vpn clients
Write-Host "Creating `"" -NoNewline
Write-Host "Internal" -NoNewline -ForegroundColor Yellow
Write-Host "`" Security Group... " -NoNewline
$internalSecurityGroupID = New-EC2SecurityGroup -GroupName $projectName"-internal" -GroupDescription "Describes traffic originating from the $($projectName) lab." -VpcId $newVPC.vpcid
$internalSecurityGroup = Get-EC2SecurityGroup -GroupId $internalSecurityGroupID

# Check for correct operation and report
if (!$internalSecurityGroup){
    Write-Error "Couldn't create internal security group, traffic will be restricted"
}
else {
    done
}

# If the user hasn't specified a source CIDR ($sourceCIDR) we can prompt for this now, checking for valid input
if (!$sourceCIDR){

    write-host "Autodetecting your Public IP... " -NoNewline
    $autoSourceIP = (Invoke-WebRequest -uri "http://ifconfig.me/ip").Content
    $autoSourceCIDR = $autoSourceIP+"/32"
    if ($autoSourceCIDR){
        Write-Host $autoSourceCIDR" " -NoNewline -ForegroundColor Yellow
        done
    }
    else {
        Write-Error "Unable to detect source, `"ifconfig.me/ip`" service offline?"
    }
    
    # Start to define the array options
    $option1 = "Proceed without source IP"
    $option2 = "Add manually configured source IP now"

    # Initialise the SourceCIDR variable (as an arraylist) with the option to create new
    [System.Collections.ArrayList]$sourceCIDRlist = @($option1)

    # Add the option for someone to specify a source CIDR interactively
    $null = $sourceCIDRlist.Add($option2)

    if ($autoSourceCIDR){
        # ...and the autodetected CIDR too
        $option3 = "Add $($autoSourceCIDR) as the configured source"

         # Add the option for the autodetected source IP (this workstation)
         $null = $sourceCIDRlist.Add($option3)
    }

    # Show the menu of choices from the source CIDR arraylist
    Write-Host "Please select an option for granting inbound access to the lab."
    show-arrayMenu(@($sourceCIDRlist))

    # Parse the options and set the CIDR appropriately
    switch ($object) {
        $option1 {$sourceCIDR = "0.0.0.0/32"}
        $option2 {
            do {$sourceCIDR = Read-Host "Enter a source IP CIDR, such as 88.96.93.174/32 or 132.45.23.0/28"
                }
            until (Select-String -Pattern "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/(3[0-2]|[1-2][0-9]|[0-9]))$" -InputObject $sourceCIDR)
        }
        $option3 {$sourceCIDR = $autoSourceCIDR}
        Default {}
    }    
}

# Create a simple CIDR based rule that allows all internal comms
Write-Host "Creating simple any/any rule for inbound traffic to the lab, from the lab, vpn and any defined source CIDR ranges... " -NoNewline
$vpnRule = @{IpProtocol="-1"; FromPort="-1"; ToPort="-1"; IpRanges=$vpnCIDR}
$vpcRule = @{IpProtocol="-1"; FromPort="-1"; ToPort="-1"; IpRanges=$vpcCIDR}
$sourceCIDRHTTPSRule = @{IpProtocol="6"; FromPort="443"; ToPort="443"; IpRanges=$sourceCIDR}
$sourceCIDRPrismRule = @{IpProtocol="6"; FromPort="9440"; ToPort="9440"; IpRanges=$sourceCIDR}
$sourceCIDRHTTPRule = @{IpProtocol="6"; FromPort="80"; ToPort="80"; IpRanges=$sourceCIDR}
$sourceCIDRSSHRule = @{IpProtocol="6"; FromPort="22"; ToPort="22"; IpRanges=$sourceCIDR}

Grant-EC2SecurityGroupIngress -GroupId $internalSecurityGroup.GroupId -IpPermission @($vpnRule,$vpcRule,$sourceCIDRHTTPSRule,$sourceCIDRHTTPRule,$sourceCIDRPrismRule,$sourceCIDRSSHRule)
done

# Create and Assign tags to new security group
Write-Host "Assigning Name Tag... " -NoNewline
$tag = New-Object Amazon.EC2.Model.Tag
    $tag.Key = "Name"
    $tag.Value = $projectName+"-internal"

# And assign it to the security group
New-EC2Tag -Resource $internalSecurityGroup.GroupId -Tag $tag
done

# Check for correct creation
if (!(Get-EC2SecurityGroup | Where-Object {$_.tags.value -eq "$($projectName)-internal"})){
    Write-Error "There was a problem creating/tagging the security group, traffic will be restricted"
}

# Insert line spacing
Write-Host ""

# Create t2.micro instance (free tier) that you can log into, and run tests from
Write-Host "Creating t2.micro instance we can use to work from, inside the VPC... " -NoNewline
$imageID = Get-SSMLatestEC2Image -Path ami-amazon-linux-latest -ImageName amzn2-ami-hvm-x86_64-gp2
$subnetId = (Get-EC2Subnet | Where-Object{$_.tags.value -eq "user-vms" -and $_.vpcid -eq $newVPC.vpcid}).subnetId
$newInstance = New-EC2Instance -ImageId $imageID -MinCount 1 -MaxCount 1 -SubnetId $subnetId -InstanceType t2.micro -KeyName ($keyPair).keyName -SecurityGroupId $internalSecurityGroup.GroupId
if (!$newInstance){
    Write-Error "Failed to create new instance, terminating" -ErrorAction Stop
}
else {
    done
}

# Wait for the instance to hit the running state
Write-Host "Waiting for instance to come up... "
Test-ObjectStatus -id $newInstance.instances[0].instanceid -wait -for running -mins 2

# Tag the instance with the project Name
Write-Host "Assigning Name Tag... " -NoNewline
$tag = New-Object Amazon.EC2.Model.Tag
    $tag.Key = "Name"
    $tag.Value = $ec2LinuxVMname

# And assign it to the instance
New-EC2Tag -Resource $newInstance.instances[0].instanceid -Tag $tag
done

# Create a FQDN for the instance in the R53 hosted zone
Write-Host "Creating DNS entry... " -NoNewline
$id = (Get-R53HostedZones | Where-Object{$_.name -eq "$($domainName)."}).id
$ip = $newInstance.instances[0].privateIPaddress
$fqdn = "$($ec2LinuxVMname).$($domainName)"
Write-Host $fqdn -NoNewline -ForegroundColor Yellow
Write-Host " > " -NoNewline
Write-Host $ip" " -NoNewline -ForegroundColor Yellow 

# Use the function in the module file to automate this
New-R53HostedZoneRecord -id $id -type A -name $fqdn -resource $ip -quiet
done

# Insert line spacing
Write-Host ""

# By this point, we should have either a valid CIDR from the initial script params, or from interactive prompt, or, no source at all (0.0.0.0/32 is a bit of a fudge, but will prevent inbound matches)

<# Create a security group for the load balancer which will allow inbound access from the source CIDR
Write-Host "Creating `"" -NoNewline
Write-Host "Inbound Load Balancer" -NoNewline -ForegroundColor Yellow
Write-Host "`" Security Group... " -NoNewline
$inboundELBSecurityGroupID = New-EC2SecurityGroup -GroupName $projectName"-elb" -GroupDescription "Describes traffic originating from the $($projectName) lab." -VpcId $newVPC.vpcid
$inboundELBSecurityGroup = Get-EC2SecurityGroup -GroupId $inboundELBSecurityGroupID

# Check for correct operation and report
if (!$inboundELBSecurityGroup){
    Write-Error "Couldn't create internal security group, traffic will be restricted"
}
else {
    done
}

# Create a simple CIDR based rule that allows all inbound comms from specified source CIDR range, to HTTP, HTTPs and 9440 for Prism Access
Write-Host "Creating simple HTTPS rule for inbound traffic to the ELB, from the specified source CIDR range... " -NoNewline
$inboundELBRule1 = @{IpProtocol="6"; FromPort="443"; ToPort="443"; IpRanges=$sourceCIDR}
$inboundELBRule2 = @{IpProtocol="6"; FromPort="9440"; ToPort="9440"; IpRanges=$sourceCIDR}
$inboundELBRule3 = @{IpProtocol="6"; FromPort="80"; ToPort="80"; IpRanges=$sourceCIDR}
Grant-EC2SecurityGroupIngress -GroupId $inboundELBSecurityGroupID -IpPermission @($inboundELBRule1,$inboundELBRule2,$inboundELBRule3)
done

# Create and Assign tags to new security group
Write-Host "Assigning Name Tag... " -NoNewline
$tag = New-Object Amazon.EC2.Model.Tag
    $tag.Key = "Name"
    $tag.Value = $projectName+"-elb"

# And assign it to the security group
New-EC2Tag -Resource $inboundELBSecurityGroupId -Tag $tag
done

# Check for correct creation
if (!(Get-EC2SecurityGroup | Where-Object {$_.tags.value -eq "$($projectName)-elb"})){
    Write-Error "There was a problem creating/tagging the security group, inbound traffic to the ELB will be restricted"
}
#>

# Create a new style "network" loadbalancer, this doesn't need redundant subnets in different AZs (like the 'application' type do)
Write-Host "Deploying new ELB (v2) for use in mapping to internal services... " -NoNewline
$inboundELB = New-ELB2LoadBalancer -name $projectName -type network -subnet $uplinkSubnet.subnetId #-SecurityGroup $inboundELBSecurityGroupId

if (!$inboundELB){
    Write-Error "Couldn't create ELB, no inbound access to services will be possible"
}
else {
    done
}

# Creating old style (classic ELB, for SSH access to the EC2 instance we deployed)
Write-Host "Creating Classic ELB for SSH access to Linux instance in EC2... " -NoNewline
<#
# Create the ELB listeners
$httpListener = New-Object Amazon.ElasticLoadBalancing.Model.Listener
    $httpListener.Protocol = "http"
    $httpListener.LoadBalancerPort = 80
    $httpListener.InstanceProtocol = "http"
    $httpListener.InstancePort = 80

$9440Listener = New-Object Amazon.ElasticLoadBalancing.Model.Listener
    $9440Listener.Protocol = "tcp"
    $9440Listener.LoadBalancerPort = 9440
    $9440Listener.InstanceProtocol = "tcp"
    $9440Listener.InstancePort = 9440

$httpsListener = New-Object Amazon.ElasticLoadBalancing.Model.Listener
    $httpsListener.Protocol = "tcp"
    $httpsListener.LoadBalancerPort = 443
    $httpsListener.InstanceProtocol = "tcp"
    $httpsListener.InstancePort = 443
#>

$sshListener = New-Object Amazon.ElasticLoadBalancing.Model.Listener
    $sshListener.Protocol = "tcp"
    $sshListener.LoadBalancerPort = 22
    $sshListener.InstanceProtocol = "tcp"
    $sshListener.InstancePort = 22

# Creating a "classic" LB for SSH access to the Linux instance
$elbName = "$($projectName)-linuxEC2"
$classicELB = New-ELBLoadBalancer -LoadBalancerName $elbName -SecurityGroup $internalSecurityGroupID -Subnet $uplinkSubnet.subnetId -Listener $sshListener
if (!$classicELB){
    Write-Error "Cannot create load balancer - inbound SSH access to linux instance in EC2 won't work, cannot continue" -ErrorAction Stop
}
else{
    done
}

# Adding the EC2 Linux instance to the SSH Load Balancer
Write-Host "Adding EC2Linux instance to the classic load balancer... " -NoNewline
$elbRegistration = Register-ELBInstanceWithLoadBalancer -Instance $newInstance.instances[0].instanceid -LoadBalancerName $elbName
if (!$elbRegistration){
    Write-Error "Couldn't register EC2 Linux instance with ELB"
}   
else{
    done

    Write-Host "You can now use `"ssh -i yourKeyPair ec2-user@$($classicelb)`" to connect to the instance"
    Write-Host "Where your keypair was called `"$($keyPair.keyName)`", and assuming your source IP is permitted"
}

# Insert line spacing
Write-Host ""

# Instruct users to start the process of building their Cluster in NCA console
Write-Host "********************************************************************************" -ForegroundColor Yellow
Write-Host "You can start to deploy your Nutanix Clusters solution now, using my.nutanix.com"
Write-Host "Proceed once the cluster has started deployment"
Write-Host "********************************************************************************" -ForegroundColor Yellow

# Insert pause
Pause

# Insert line spacing
Write-Host ""

<#

# This command will use the specified keypair file and the ELB DNS Name, to install git onto the EC2 instance
ssh ec2-user@$($classicELB) -i mykey.pem "sudo yum install git -y"

# This command will then clone the Easy-RSA git repo
ssh ec2-user@$($classicELB) -i mykey.pem "git clone https://github.com/OpenVPN/easy-rsa.git"

# This command will initialise the EasyRSA install
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa init-pki"             

# This command will then create the CA certificate
ssh ec2-user@$($classicELB) -i mykey.pem "echo |easy-rsa/easyrsa3/./easyrsa build-ca nopass"

# This command will build the openVPN server certificate
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa build-server-full server nopass"

# This command will build a client certificate
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa build-client-full $($projectName) nopass" 

# This command will copy the ca.crt back to your local machine, using SCP
scp -i mykey.pem ec2-user@$($classicELB):pki/ca.crt .

# This command will copy the server.crt and the client crt file (name based on project) back to your local machine, using SCP
scp -i mykey.pem ec2-user@$($classicELB):pki/issued/*.crt .

# This command will copy the private key files of the ca, server and client to your local machine, using SCP
scp -i mykey.pem ec2-user@$($classicELB):pki/private/*.key .

#>

$keyGenerationInstructions = @"
The below has been copied to your clipboard, proceed when you've
carried out the task and have the certificates & keys locally.
----------------------------------------------------------------

ssh ec2-user@$($classicELB) -i mykey.pem "sudo yum install git -y"
ssh ec2-user@$($classicELB) -i mykey.pem "git clone https://github.com/OpenVPN/easy-rsa.git"
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa init-pki"             
ssh ec2-user@$($classicELB) -i mykey.pem "echo |easy-rsa/easyrsa3/./easyrsa build-ca nopass"
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa build-server-full $($projectName)-server nopass"
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa build-client-full $($projectName)-client nopass"

Linux/Mac Users:
In a new Terminal Tab, Run the above commands, it'll open an SSH tunnel and execute the commands in quotes.

Windows Users:
For putty, login using the key file, and then execute the commands in quotes above, starting at "sudo yum install..."

ssh ec2-user@$($classicELB) -i mykey.pem "sudo yum install git -y"
ssh ec2-user@$($classicELB) -i mykey.pem "git clone https://github.com/OpenVPN/easy-rsa.git"
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa init-pki"             
ssh ec2-user@$($classicELB) -i mykey.pem "echo |easy-rsa/easyrsa3/./easyrsa build-ca nopass"
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa build-server-full $($projectName)-server nopass"
ssh ec2-user@$($classicELB) -i mykey.pem "easy-rsa/easyrsa3/./easyrsa build-client-full $($projectName)-client nopass" 


Now you've generated all the certs you need, bring them back to your workstation so you can upload them to AWS.

scp -i mykey.pem ec2-user@$($classicELB):pki/ca.crt .
scp -i mykey.pem ec2-user@$($classicELB):pki/issued/*.crt .
scp -i mykey.pem ec2-user@$($classicELB):pki/private/*.key .

Windows Users, you'll need to use PSCP (or equivalent) to pull back the files above,
Store them in the same folder this script is executing from
"@

Write-Host $keyGenerationInstructions -ForegroundColor DarkGray
$keyGenerationInstructions | Set-Clipboard
Pause

# Insert line spacing
Write-Host ""

# Use Import-ACMcertificate to import the certs to the store
Write-Host "Importing Certs from local filesystem..."
$caBytes = [System.IO.File]::ReadAllBytes("$(((Get-Location).path))/ca.crt")
$serverCertBytes = [System.IO.File]::ReadAllBytes("$(((Get-Location).path))/$($projectName)-server.crt")
$serverPkBytes = [System.IO.File]::ReadAllBytes("$(((Get-Location).path))/$($projectName)-server.key")
$clientCertBytes = [System.IO.File]::ReadAllBytes("$(((Get-Location).path))/$($projectName)-client.crt")
$clientCert = Get-Content -path ("$(((Get-Location).path))/$($projectName)-client.crt") -tail 20
$clientPkBytes = [System.IO.File]::ReadAllBytes("$(((Get-Location).path))/$($projectName)-client.key")
$clientKey = Get-Content -Path "$(((Get-Location).path))/$($projectName)-client.key"
if (!($caBytes -and $serverCertBytes -and $serverPkBytes -and $clientPkBytes -and $clientCertBytes)){
    Write-Error "Unable to read one or more of the certs, are they stored locally? Cannot continue." -ErrorAction Stop
}
done

Write-Host "Uploading certs to Amazon ACM..."
$serverCertARN = Import-ACMCertificate -Certificate $serverCertBytes -PrivateKey $serverPkBytes -CertificateChain $caBytes
$clientCertARN = Import-ACMCertificate -Certificate $clientCertBytes -PrivateKey $clientPkBytes -CertificateChain $caBytes
if (!($serverCertARN -and $clientCertARN)){
    Write-Error "Unable to upload one or more of the certs. Cannot continue." -ErrorAction Stop
}
done

# Create the VPN Client Endpoint authentication type (mutual certs)
Write-Host "Building the Client VPN Endpoint..."
$authOption = New-Object Amazon.ec2.model.clientvpnauthenticationrequest
$authOption.Type = "certificate-authentication"
$authOption.MutualAuthentication = New-Object Amazon.EC2.Model.CertificateAuthenticationRequest

# Put the client cert in the auth options object
$authOption.MutualAuthentication.ClientRootCertificateChainArn = $clientCertARN

# Build the Client VPN Endpoint
$vpnClientEndpoint = New-EC2ClientVpnEndpoint -AuthenticationOption $authoption -ClientCidrBlock $vpnCIDR -Description $projectName -DnsServer $nameServer -ServerCertificateArn $serverCertARN -VpcId $newVPC.vpcid -ConnectionLogOptions_Enabled $false -splitTunnel $true -SecurityGroupId $internalSecurityGroup.GroupId -selfServicePortal disabled
if (!$vpnClientEndpoint){
    Write-Error "VPN Configuration Failed - Review for manual steps"
    Write-Host "https://docs.aws.amazon.com/vpn/latest/clientvpn-admin/cvpn-getting-started.html"
}
else {
        # Wait for it to be ready for associating
        Test-ObjectStatus -id $vpnClientEndpoint.ClientVpnEndpointId -wait -for pending-associate
        done
    
        # Associate the Client VPN Endpoint with the uplink subnet
        Write-Host "Associating VPN Endpoint with the Uplink Subnet (this can take some time)... "
        Register-EC2ClientVpnTargetNetwork -ClientVpnEndpointId $vpnClientEndpoint.ClientVpnEndpointId -SubnetId $uplinkSubnet.SubnetId
        Test-ObjectStatus -id $vpnClientEndpoint.ClientVpnEndpointId -wait -for available -mins 15 -quiet
        done
    
        # Grabbing the client VPN config needed
        Write-Host "Exporting Client VPN Config file... " -NoNewline
        $vpnConfigFile = "$($projectName).ovpn"
        Write-Host $vpnConfigFile -NoNewline -ForegroundColor Yellow
        $clientVPNconfig = Export-EC2ClientVpnClientConfiguration -ClientVpnEndpointId $vpnClientEndpoint.ClientVpnEndpointId
        $clientVPNconfig | Out-File -FilePath $vpnConfigFile
    
        # Check for correct operation
        if (!(Test-Path $vpnConfigFile)){
            Write-Error "Couldn't write the file, check permissions on the directory this script is running from"
        }
        else {
            done
        }
    
        # Grant EC2 Client VPN Authorization Rule / Ingress Rule
        Write-Host "Enabling VPN Authorization to the whole VPC... "
        $vpcAuthorizationRule = Grant-EC2ClientVpnIngress -TargetNetworkCidr $vpcCIDR -AuthorizeAllGroup $true -ClientVpnEndpointId $vpnClientEndpoint.ClientVpnEndpointId
        # Test for correct application
        if ($vpcAuthorizationRule.Code.Value -ne "authorizing" -or $vpcAuthorizationRule.Code.Value -ne "active"){
            Write-Error "Couldn't automatically add the authorization rule for remote clients accessing the VPC - this needs fixing"
        }
        else {
            done
        }
    
    
        # Fixing up the OpenVPN config file to include the certificate and key

        # Format the data so it can be imported into the config file
        $insert = @"
        <cert>
        $($clientCert)
        </cert>
        <key>
        $($clientKey)
        </key>
"@

        # Fix some new-line issues
        $insert = $insert.Replace('BEGIN CERTIFICATE','BEGINCERTIFICATE')
        $insert = $insert.Replace('END CERTIFICATE','ENDCERTIFICATE')
        $insert = $insert.Replace('BEGIN PRIVATE KEY','BEGINPRIVATEKEY')
        $insert = $insert.Replace('END PRIVATE KEY','ENDPRIVATEKEY')
        $insert = $insert.Replace(" ","`n")
        $insert = $insert.Replace('BEGINCERTIFICATE','BEGIN CERTIFICATE')
        $insert = $insert.Replace('ENDCERTIFICATE','END CERTIFICATE')
        $insert = $insert.Replace('BEGINPRIVATEKEY','BEGIN PRIVATE KEY')
        $insert = $insert.Replace('ENDPRIVATEKEY','END PRIVATE KEY')

        # Get the downloaded config file and insert the new text
        $vpnConfig = Get-Content $vpnConfigFile
        $vpnConfig = $vpnConfig + $insert

        # Add some random content to the dns name, as per the instructions
        $vpnConfig = $vpnConfig.Replace("cvpn-endpoint","$(get-random).cvpn-endpoint")

        # Over-write the downloade config
        $vpnConfig | Out-File $vpnconfigfile -Force 
    
        # Notify the user that they can attempt to connect using their OpenVPN Client
        Write-Host "You can proceed to download/install the OpenVPN client for your workstation"
        Write-Host "import the profile file, $($vpnConfigFile) to connect."
    
        # Create VPN Gateway to route traffic to/from the internet and associate with the VPC
        $newVPNGateway = New-EC2VpnGateway -Type ipsec.1
        Write-Host "Checking VPN GW Availability..."
        Test-ObjectStatus -id $newVPNGateway.VpnGatewayId -wait -for available
        $vpcVPNgateway = Add-EC2VpnGateway -VpcId $newVPC.VpcId -VpnGatewayId $newVPNGateway.VpnGatewayId
        if (!$vpcVPNgateway){
            Write-Error "Something stopped us from attaching the VPN gateway to the VPC."
        }
        else {
            done
        }
}

# Create and map listeners to the ELB $inboundELB

#>
