<#
.SYNOPSIS
MODULE WRITTEN BY : Dave Hocking, Cloud Architect, Nutanix GSI Team, UK
MODULE WRITTEN ON : First created, April 21st 2021

This module was written to capture some of the most common tasks that
are carried out when working on AWS infrastructure, with the goal of
creating a simple lab environment. Nutanix can make use of these labs
so that demonstrations of our "Clusters in AWS" technology can be 
showcased. This technology deploys Nutanix onto AWS bare metal, so that
a private cloud can be created inside an Amazon VPC.

To demonstrate this technology, it's easier if some basic lab constructs
exist, such as DNS (with local resolution), outbound internet access,
some subnets to segment traffic onto, a VPN server (for client access).

This module file (.psm1) contains the commonly used functions, while the
accompanying scripts (.ps1) are used to carry out the more complex tasks
like building the lab, and removing it.

.DESCRIPTION

File Descriptions
=================

FILENAME            PURPOSE
aws-autolab.psm1    This file, contains functions used in the scripts below
new-lab.ps1         This script creates the lab environment
remove-lab.ps1      This script is responsible for cleaning up


Function Descriptions
=====================

FUNCTION            PURPOSE
done                Just used for script output formatting
Show-ArrayMenu      Takes an array of supplied objects, and presents a choice menu
Confirm-Continue    Interactive prompt, for user to decide if they continue or not
Initialise-Module   Works with AWS credentials (required) to create a profile
Test-ObjectStatus   Supply an AWS object, such as NAT gateway, and test for status


.LINK
https://bitbucket.org/davehocking/aws-autolab/src/main/

#>

# Create outputting function to save on lines
function done(){
    Write-Host "Done" -ForegroundColor Green
}

function show-arrayMenu()
{
    <#
    .SYNOPSIS
        version 1.0 - Written by Dave Hocking 2021.
        Function takes an array of objects (such as from "ls" or "get-module")...
        Presents them as a menu with a numerical reference to choose from...
        Then returns a global-scoped variable called $object that contains the choice

    .EXAMPLE
        show-ArrayMenu (get-module)
         - Lists all loaded PS modules, then displays a list for the user to choose from

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    $objects
    )

    # Define the do-while loop that cycles around until a valid choice is made
    do {
        $loop = $false
        $numObjects = $objects.count

        # Starts for-loop to draw menu based on the number of items in the array
        for($i=1; $i -le $numObjects;$i++)
        {
            Write-Host "(" -NoNewline -ForegroundColor Gray
            Write-Host "$i" -NoNewline -ForegroundColor Green
            Write-Host ") " -NoNewline -ForegroundColor Gray
            Write-Host $objects.get($i-1)
        }

        # Give the user an option for (x) to leave this menu (return NO $object)
        Write-Host "(" -NoNewline -ForegroundColor Gray
        Write-Host "x" -NoNewline -ForegroundColor Red
        Write-Host ") " -NoNewline -ForegroundColor Gray
        Write-Host eXit this menu

        # Prompts user to choose an option, then verifies it's validity
        ""
        $choice = Read-Host "Enter your selection (1-n) or (x)"
        if ($choice -eq "x" -or $choice -eq "X")
        {
          break
        }

        else
        {
            # if the choice wasn't to quit, decide if it's in bounds
            [int32]$selection = $choice

            if ($selection -gt $numObjects -or $selection -lt 1)
            {
                # Send the user around the loop again, until they choose a valid option
                Write-Warning "Not a valid choice, please try again."
                ""
                $loop = $true
            }
        }
    }
    while ($loop -eq $true)

    # Error checking
    if (!$selection)
    {
        Write-Warning "Something went wrong, causing no selection to be detected."
        break
    }
    else
    {
        # Output the object of the users' choice
        $global:object = $objects.get($selection-1)
        Write-Host "Selected " -NoNewline
        Write-Host "$($object)" -ForegroundColor Green
    }
}

function confirm-continue()
{
    <#
        .SYNOPSIS
        Manages user-input for a simple confirm to continue prompt
        .DESCRIPTION
        This function draws a simple "yes / no" dialogue box for users to confirm
        The function exits and creates script-scoped variables for $continue or $break
        Use of this function should include the ability to detect the presence of the $break variable
        and instructions to break from whatever processing was taking place.

        i.e.

            confirm-continue
            if ($break)
            {
                break
            }

        .NOTES
        Author:  Dave Hocking
        .EXAMPLE
        PS> confirm-continue
    #>

    do
    {
        $global:break = $false
        $loop = $false
        $continue = Read-Host "Do you wish to continue y/n ?"

        # Uses the switch statement to check for the input
        switch ($continue)
        {
            y {$loop = $false; $script:continue}
            n {Write-host "Stopping action, on command." -ForegroundColor Yellow; $global:break = $true}
            default {"You must choose an option, y or n" ; "" ; $loop = $true}
        }
    }
    while ($loop -eq $true)
}

function initialize-module()
{
    # Check for PowerShell Core
    Write-Host "Checking for correct PowerShell Edition..."
    if ($PSVersionTable.PSEdition -eq "Core"){
        done
    }
    else {
        Write-Warning "PowerShell Core Edition not detected."
        Write-Error "Not supported on PS Edition:"$PSVersionTable.PSEdition -ErrorAction Stop
    }

    # Import the PS core module for AWS if required
    Write-Host "Checking for AWS PowerShell Core Support... " -NoNewline
    if (Get-Module -Name awspowershell.netcore){
        done
        Write-Host "Module exists... " -NoNewline

        $awsModule = Get-Module -Name awspowershell.netcore
        Write-Host $awsModule.Version -ForegroundColor Green
    }
    else {
        done
        Write-Host "Importing " -NoNewline
        Write-Host "AWS" -NoNewline -ForegroundColor Yellow
        Write-Host " Powershell Core Module" -NoNewline
        Import-Module awspowershell.netcore -Global    

        # Check for success
        $awsModule = Get-Module -Name awspowershell.netcore
        if (!$awsModule){
            Write-Error "Couldn't load AWS modules - aborting" -ErrorAction Stop
        }
        else {
            Write-Host "Loaded Version " -NoNewline
            Write-Host $awsModule.Version -ForegroundColor Green
        }
    }

    # Insert a break
    Write-Host ""

    # Let's offer the option to wipe out the existing creds
    Write-Host "If you continue here, we will " -NoNewline
    Write-Host "destroy" -NoNewline -ForegroundColor Red
    Write-Host " any stored 'default' profile for AWS credentials."
    Write-Host "Select 'n' if you want to use existing details."
    confirm-continue
    if (!$break){
        Remove-AWSCredentialProfile -ProfileName default -Force
        
        # Now no creds exists, set them
        $AWS_ACCESS_KEY_ID = Read-Host -Prompt "Access Key ID"
        $AWS_SECRET_ACCESS_KEY = Read-Host -Prompt "Access Key Secret" -AsSecureString
        $AWS_SESSION_TOKEN = Read-Host -Prompt "Session Token" -AsSecureString

        # Setting Default credentials
        Write-Host "Setting AWS credentials... " -NoNewline
        Set-AWSCredential -AccessKey $AWS_ACCESS_KEY_ID -SessionToken (ConvertFrom-SecureString $AWS_SESSION_TOKEN -AsPlainText) -SecretAccessKey (ConvertFrom-SecureString $AWS_SECRET_ACCESS_KEY -AsPlainText) -StoreAs default 
    }
    
    # Get the default creds
    $creds = Get-AWSCredential -ListProfileDetail | Where-Object {$_.ProfileName -eq "default"}

    if (!$creds) {
        Write-Error "Couldn't load/create credentials, aborting" -ErrorAction Stop
    }
    else {
        done
        Write-Host ""

        # Setting the default configuration and region
        if (!(Get-AWSRegion | Where-Object {$_.IsShellDefault -ne $False})){
            Write-Host "No Default Region stored in profile, listing choices"
            $regions = Get-AWSRegion | Select-Object -Property Region,Name
            show-arrayMenu($regions.name)

            # Extract the region name from the location chosen by the user above
            $global:region = ($regions | Where-Object {$_.Name -eq $object}).region
            
            Write-Host "Setting Region as " -NoNewline
            Write-Host $region" " -ForegroundColor Yellow -NoNewline
            Initialize-AWSDefaultConfiguration -ProfileName default -Region $region
            done
        }
    }
}

function Test-ObjectStatus()
{
    <#  .SYNOPSIS
        version 0.1 - Written by Dave Hocking 2021.

        This function will check (and report on) or wait for a specifed object's state.
        If the operation is to "wait" for the state specified, you can set a timeout value
        If the timeout value is exceeded and the desired state hasn't changed, an error is thrown

        Requires RestAPI connection to AWS - see "initialise-module"
        Requires an "id" for the object in AWS, such as NAT Gateway or VPN Client Endpoint - returned when you submit a "create" request

        .DESCRIPTION
        Here are the possible objects you can check for:

        NAME	             DESCRIPTION
        natgw                NAT Gateway.
        vpnEndpoint          A VPN Client Endpoint.
        vpnGateway           A VPN Client Gateway.
        vpnAssocStat         VPN Association Status. *WIP
        elb2                 A new style Elastic Load Balancer 
        ec2instance          An EC2 instance.

        * NB: This is imperfect with the EC2 instances as their status doesn't follow the same schema as the VPC objects

        * NB: For ELB2 objects, use the LoadBalancerArn of the instance, they don't have id's like the other objects do
        
        
        Here are the possible states you can check for, and what object type they're valid as:

        STATE                DESCRIPTION                                                VALID STATE FOR OBJECT
        provisioning         The object is being created                                ELB2 Load Balancer
        active               The object is running                                      ELB2 Load Balancer
        active_impaired      The object is running, but cannot be scaled                ELB2 Load Balancer
        failed               The object could not be created                            ELB2 Load Balancer
        pending-associate    The endpoint is waiting to be associated to a subnet/vpc   VPN Client Endpoint
        Associated           The object has associated correctly                        VPN Association
        Associating          The object is associating                                  VPN Association
        AssociationFailed    The object association failed                              VPN Association
        Disassociated        The object has disassociated                               VPN Association
        Disassociating       The object is disassociating                               VPN Association
        pending              The object is scheduled to be created.                     NAT Gateway/EC2 Instance
        available            The object is runnning.                                    NAT Gateway/VPN Client Endpoint
        deleting             A deletion request for the object has been receieved.      NAT Gateway/VPN Client Endpoint
        deleted              The object has been destroyed.                             NAT Gateway/VPN Client Endpoint
        initializing         The object is being created.                               EC2 Instance
        running              The object is in a normal, running state.                  EC2 Instance
        terminated           The object has been deleted.                               EC2 Instance
        shutting-down        The object is being terminated.                            EC2 Instance


        .EXAMPLE
        If you want to script a task that should wait 5 mins for the object at $id to switch to "Available":
        Test-ObjectStatus $id -wait -for available -mins 5

        .EXAMPLE
        If you want to script a task that should wait 2 mins for the object at $id to switch to "deleted":
        Test-ObjectStatus $id -wait -for deleted -mins 2 -quiet
         - The quiet switch will prevent periodic reporting of the object status

        .EXAMPLE
        If you wanted to return $true or $false for the status or "pending":
        Test-ObjectStatus $id -check -for pending

        .EXAMPLE
        If you wanted to simply report the object state given it's $id:
        Test-ObjectStatus $id -report
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    # Require a target id
    [Parameter(Position=0,Mandatory=$True)]
    $id
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Wait")]
    [switch]$wait
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Check")]
    [switch]$check
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Report")]
    [switch]$report
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Check")]
    [Parameter(Mandatory=$true,ParameterSetName="Wait")]
    [ValidateSet("Disassociated","Disassociating","Associated","Associating","AssociationFailed","active","active_impaired","failed","provisioning","pending","pending-associate","initializing","available","deleting","deleted","running","terminated","terminating")]
    [string]$for
    ,
    # Validate that any -mins specified are with the range below
    [Parameter(ParameterSetName="Wait")]
    [ValidateRange(1,60)]
    [Int]$mins
    ,

    # Optional -quiet switch to prevent %-status reporting
    [Parameter(ParameterSetName="Wait")]
    [switch]$quiet
    )

    # Re-write variable for clarity
    $desiredStatus = $for

    # Determine from the "id" what type of object we want to test for, and set $testObject
    switch -regex ($id) {
        "nat" {$testObject = "natgw"}
        "vgw" {$testObject = "vpnGateway"}
        "cvpn-endpoint" {$testObject = "vpnEndpoint"}
        "cvpn-assoc-" {$testObject = "vpnAssocStat"}
        "i-" {$testObject = "ec2instance"}
        "arn:aws:elasticloadbalancing:" {$testObject = "elb2"}
        Default {}
    }

    # If there's not been a match above, break with error
    if (!$testObject){
        Write-Error "Couldn't detect any object from the `$id given, check for support types and correct inputs" -ErrorAction Stop
    }

    # Check that the object is valid and can be detected
    switch ($testObject) {
        "natgw" {$objectStatus = (Get-EC2NatGateway -NatGatewayId $id).state}
        "vpnGateway" {$objectStatus = (Get-EC2VPNGateway -VpnGatewayId $id).state}
        "vpnEndpoint" {$objectStatus = (Get-EC2ClientVpnEndpoint -ClientVpnEndpointId $id).status.code.value}
        #"vpnAssocStat" {$objectStatus = (Get-??? -ClientVpnEndpointId $id).status.code.value}
        "ec2instance" {$objectStatus = (Get-EC2instance -InstanceId $id).instances[0].state.name.value}
        "elb2" {$objectStatus = (Get-ELB2LoadBalancer | Where-Object {$_.LoadBalancerArn -eq $id}).state.code.value}
        Default {}
    }
    
    # If there's not been a match above, break with error
    if (!$objectStatus){
        Write-Error "Couldn't detect a valid object from the `$id given, check for correct input. Note that for ELB2's this should be the ARN" -ErrorAction Stop
    }

    # Set up the "check" operation
    if ($check) {
        if ($objectStatus -eq $desiredStatus){
            return $true
        }
        else {
            return $false
        }
    }

    # Set up the "report" operation
    if ($report) {
        Write-Host "The object status is `"" -NoNewline
        Write-Host "$($objectStatus)" -ForegroundColor Yellow -NoNewline
        Write-Host "`"" 
    }

    # Set up the "wait" operation
    if ($wait)
    {  
        # If not in "quiet" mode
        if (!$PSBoundParameters.ContainsKey("quiet")) {
            Write-Host "Waiting for object status of " -NoNewline
            Write-Host "$desiredStatus" -ForegroundColor Yellow -NoNewline
            Write-Host "..."
            }

        # Set interval between tests
        $intervalSecs = 5

        # If $mins hasn't been populated, use a default of 5
        if (!$mins)
        {
            $mins = 5
        }

        # Determine the correct number of iterations to run through
        $iterations = ($mins*60)/$intervalSecs

        # Create loop to run for the specified length of time
        for ($i=1; $i -le $iterations; $i++)
        {
            start-sleep $intervalSecs

            # Get the appropiate object, based on the specified type
            switch ($testObject) {
                "natgw" {$objectStatus = (Get-EC2NatGateway -NatGatewayId $id).state}
                "vpnGateway" {$objectStatus = (Get-EC2VPNGateway -VpnGatewayId $id).state}
                "vpnEndpoint" {$objectStatus = (Get-EC2ClientVpnEndpoint -ClientVpnEndpointId $id).status.code.value}
                "ec2instance" {$objectStatus = (Get-EC2instance -InstanceId $id).instances[0].state.name.value}
                "elb2" {$objectStatus = (Get-ELB2LoadBalancer | Where-Object {$_.LoadBalancerArn -eq $id}).state.code.value}
                Default {}
            }

            # Check for -quiet switch and alter output as required
            if (!$PSBoundParameters.ContainsKey("quiet"))
            {
                write-host "Object Status is: `"" -NoNewline
                Write-Host "$($objectStatus)" -ForegroundColor Yellow -NoNewline
                Write-Host "`""
            }

            # Check if the actual object status matches any of the desiredStatus combinations
            $match = $desiredStatus.Contains($objectStatus.ToString())
            if ($match -eq $True)
            {
                $result = "complete"
                break
            }
        }

        if ($result) {
            if (!$PSBoundParameters.ContainsKey("quiet")) {
            "Desired status of `"" + $desiredStatus + "`" was reached."
            }
        }

        # If status doesn't change, display error and stop script
        else {
            write-warning "Failed to reach desired status within the time allocated."
            Write-Host "The object status is `"" -NoNewline
            Write-Host "$($objectStatus)" -ForegroundColor Yellow -NoNewline
            Write-Host "`""
           # if ($object.failurecode -or $object.failuremessage){
           #     Write-Warning $object.failurecode
           #     Write-Warning $object.failuremessage
           #     break
           # }
        }
    }
}

function New-R53HostedZoneRecord()
{
      <#  .SYNOPSIS
        version 0.1 - Written by Dave Hocking 2021.

        This function will create records in Route53 Hosted Zones as this functionality isn't covered in the standard AWS tools

        Requires RestAPI connection to AWS - see "initialise-module"

        .DESCRIPTION
        Creates, A, PTR and CNAME records in Route53 Hosted Zones.
        If an object already exists with the same name, it will be updated

        .EXAMPLE
        If you want to create an A-Record for dave.isleet.local > 10.13.3.7, using the variable $id to hold the Zone's ID:
        New-R53HostedZoneRecord -type A -name "dave.isleet.local" -resource "10.13.3.7" -id $id

        .EXAMPLE
        If you want to create an PTR-Record for 10.13.3.7 > dave.isleet.local, using the variable $id to hold the Zone's ID:
        New-R53HostedZoneRecord -type PTR -name "10.13.3.7" -resource "dave.isleet.local" -id $id

        .EXAMPLE
        If you want to create a CNAME-Record for no.n00bs.local > dave.isleet.local, using the variable $id to hold the Zone's ID:
        New-R53HostedZoneRecord -type PTR -name "no.n00bs.local" -resource "dave.isleet.local" -id $id
   
        .EXAMPLE
        If you want to create an A-Record for short.recordlife.local > 10.3.2.1, with a ttl of 10s:
        New-R53HostedZoneRecord -type A -name "dave.isleet.local" -resource "10.13.3.7" -id $id -ttl 10
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    # Require a type of record
    [Parameter(Mandatory=$true)]
    [ValidateSet("A","PTR","CNAME")]
    [string]$type
    ,
    # Grab the name for the record
    [Parameter(Mandatory=$true)]
    #[ValidateRange(1,60)] - need regex here to validate only alpha-numeric and hyphens, or IP addresses
    [string]$name
    ,
    # ..and the resource it will point at
    [Parameter(Mandatory=$true)]
    #[ValidateRange(1,60)] - need regex here to validate only alpha-numeric and hyphens, or IP addresses
    [string]$resource
    ,
    # ..and the hosted zone ID
    [Parameter(Mandatory=$true)]
    #[ValidateRange(1,60)] - need to validate correct formatting for the zoneid
    [string]$id
    ,    
    # ..and the required TTL
    [Parameter(Mandatory=$false)]
    #[ValidateRange(1,60)] - need to validate correct formatting and range for the ttl
    [int]$ttl=300
    ,
    # Optional -quiet switch to prevent over-reporting
    [switch]$quiet
    )


    $change = New-Object Amazon.Route53.Model.Change
    $change.Action = "UPSERT"
    $change.ResourceRecordSet = New-Object Amazon.Route53.Model.ResourceRecordSet
    $change.ResourceRecordSet.Name = $name
    $change.ResourceRecordSet.Type = $type
    $change.ResourceRecordSet.TTL = $TTL
    $change.ResourceRecordSet.ResourceRecords.Add(@{Value=$resource})
    
    $params = @{
        HostedZoneId=$id
        ChangeBatch_Change=$change
    }
    $update = Edit-R53ResourceRecordSet @params
    if (!$update){
        Write-Error "Couldn't update the resource record set" -ErrorAction stop
    }

}

function show-Banner()
{
    Write-Host "     _   _ _    _ _______       _   _ ______" -NoNewline -ForegroundColor Blue
    Write-Host "_   __" -ForegroundColor Green
    Write-Host "    | \ | | |  | |__   __|/\   | \ | |_   _" -NoNewline -ForegroundColor Blue
    Write-Host "\ \ / /" -ForegroundColor Green
    Write-Host "    |  \| | |  | |  | |  /  \  |  \| | | |  " -NoNewline -ForegroundColor Blue
    Write-Host "\ V / " -ForegroundColor Green
    Write-Host "    | . `` | |  | |  | | / /\ \ | . `` | | |   " -NoNewline -ForegroundColor Blue
    Write-Host "> <  " -ForegroundColor Green
    Write-Host "    | |\  | |__| |  | |/ ____ \| |\  |_| |_ " -NoNewline -ForegroundColor Blue
    Write-Host "/ . \ " -ForegroundColor Green
    Write-Host "    |_| \_|\____/   |_/_/    \_\_| \_|_____" -NoNewline -ForegroundColor Blue
    Write-Host "/_/ \_\" -ForegroundColor Green
}